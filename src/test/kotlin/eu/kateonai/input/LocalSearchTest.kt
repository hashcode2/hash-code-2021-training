package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import optimizationAlgorithms.LocalSearch
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import simpleSolutions.RandomSolution

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LocalSearchTest {

    @ParameterizedTest
    @MethodSource("test random solution optimization test cases")
    fun `test random solution optimization`(filePath: String) {
        // given
        val reader = InputParser<PizzaProblemModel>()
        // when
        val inputModel = reader.parse(filePath) {
            PizzaProblemModel()
        }
        val randomSolution = RandomSolution(inputModel);
        val localOptimalSolution = LocalSearch(randomSolution).completeLocalSearch()
        println("Utility = " + String.format("%,d", localOptimalSolution.calculateUtility()))
        // then
        assertNotNull(localOptimalSolution)
    }

    fun `test random solution optimization test cases`(): Collection<String> {
        return TestUtils.getInputFilePaths()
    }
}