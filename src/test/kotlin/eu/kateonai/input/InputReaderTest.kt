package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class InputReaderTest {

    @ParameterizedTest
    @MethodSource("input parse test cases")
    fun `input parse`(inputFilePath: String) {
        // given
        val reader = InputParser<PizzaProblemModel>()
        // when
        val res =
            reader.parse(inputFilePath) {
                PizzaProblemModel()
            }
        // then
        assertNotNull(res)
    }

    fun `input parse test cases`(): Collection<String> {
        return TestUtils.getInputFilePaths()
    }
}