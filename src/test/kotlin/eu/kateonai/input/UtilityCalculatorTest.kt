package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import eu.katonai.utility.UtilityCalculator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class UtilityCalculatorTest {

    @Test
    fun `test random solution`() {
        // given
        val filePath = this.javaClass.classLoader.getResource("a_example").path
        val reader = InputParser<PizzaProblemModel>()

        // when
        val inputModel = reader.parse(filePath){
            PizzaProblemModel()
        }

        val utilityCalculator = UtilityCalculator(inputModel)
        assertEquals(utilityCalculator.getUtilityValue(0, 1), 36)
        assertEquals(utilityCalculator.getUtilityValue(1, 3), 9)
        assertEquals(utilityCalculator.getUtilityValue(2, 3), 25)
    }
}