package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import eu.katonai.utility.UtilityCalculator
import optimizationAlgorithms.LocalSearch
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import simpleSolutions.RandomSolution
import simpleSolutions.Solution

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RandomSolutionTest {

    @ParameterizedTest
    @MethodSource("test random solution test cases")
    fun `test random solution`(inoutFilePath: String) {
        // given
        val reader = InputParser<PizzaProblemModel>()
        // when
        val inputModel = reader.parse(inoutFilePath) {
            PizzaProblemModel()
        }
        val solution = RandomSolution(inputModel)
        println("Utility = " + String.format("%,d", solution.calculateUtility()));

        // then
        assertNotNull(solution)
    }

    fun `test random solution test cases`(): Collection<String> {
        return TestUtils.getInputFilePaths()
    }
}