package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import eu.katonai.utility.UtilityCalculator
import optimizationAlgorithms.LocalSearch
import optimizationAlgorithms.VariableNeighbourhoodSearch
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import simpleSolutions.RandomSolution

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VariableNeighbourhoodSearchTest {

    @ParameterizedTest
    @MethodSource("test random solution test cases")
    fun `test random solution`(inputFilePath: String) {
        // given
        val reader = InputParser<PizzaProblemModel>()
        // when
        val inputModel = reader.parse(inputFilePath) {
            PizzaProblemModel()
        }
        val randomSolution = RandomSolution(inputModel)
        val localOptimalSolution = LocalSearch(randomSolution).completeLocalSearch()
        println("Local Search Utility = " + String.format("%,d", localOptimalSolution.calculateUtility()))
        val newSolution = VariableNeighbourhoodSearch(localOptimalSolution).performVariableNeighbourhoodSearch(100000)
        println("New Utility = " + String.format("%,d", newSolution.calculateUtility()))
        // then
        assertNotNull(newSolution)
    }

    fun `test random solution test cases`(): Collection<String> {
        return TestUtils.getInputFilePaths()
    }
}