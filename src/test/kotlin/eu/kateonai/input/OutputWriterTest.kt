package eu.kateonai.input

import eu.kateonai.input.model.PizzaProblemModel
import eu.kateonai.output.OutputWriter
import eu.kateonai.output.model.PizzaDelivery
import eu.kateonai.output.model.PizzaProblemOutputConfig
import eu.kateonai.output.model.PizzaProblemOutputModel
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import simpleSolutions.RandomSolution
import java.io.File

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OutputWriterTest {
    companion object {
        val outputFilePath = "testOutput"
    }

    @ParameterizedTest
    @MethodSource("test random solution test cases")
    fun `write random solution`(inputFilePath: String) {
        // given
        val reader = InputParser<PizzaProblemModel>()
        val inputModel = reader.parse(inputFilePath) {
            PizzaProblemModel()
        }
        val solution = RandomSolution(inputModel)
        val outputModel = PizzaProblemOutputModel().apply {
            this.config = PizzaProblemOutputConfig().apply {
                this.servedTeams = solution.servedTeams
            }
            this.deliveries = solution.pizzaDeliveries
                .map {
                    PizzaDelivery()
                        .apply {
                            this.teamMembers = it.teamMembers
                            this.pizzas = it.pizzas
                        }
                }
        }
        val writer = OutputWriter<PizzaProblemOutputModel>()
        // when
        writer.write(outputFilePath, outputModel)
        // then
        val res = File(outputFilePath)
        assertTrue(res.exists())
        res.delete()
    }

    fun `test random solution test cases`(): Collection<String> {
        return TestUtils.getInputFilePaths()
    }
}