package eu.kateonai.input

class TestUtils {
    companion object {
        fun getInputFilePaths(): Collection<String> {
            return listOf(
                this.javaClass.classLoader.getResource("a_example").path,
                this.javaClass.classLoader.getResource("b_little_bit_of_everything.in").path,
                this.javaClass.classLoader.getResource("c_many_ingredients.in").path,
                this.javaClass.classLoader.getResource("d_many_pizzas.in").path,
                this.javaClass.classLoader.getResource("e_many_teams.in").path
            )
        }
    }
}