package optimizationAlgorithms;

import eu.katonai.utility.UtilityCalculator;
import simpleSolutions.PizzaDelivery;
import simpleSolutions.Solution;

import java.util.*;

public class LocalSearch {

    private UtilityCalculator utilityCalculator;
    private Solution initialSolution;
    private Solution currentSolution;

    public LocalSearch(Solution initialSolution){
        this.utilityCalculator = initialSolution.getUtilityCalculator();
        this.initialSolution = initialSolution;
        this.currentSolution = initialSolution;
    }

    public Solution completeLocalSearch(){
        newSolution:
        for (PizzaDelivery pizzaDeliveryFirst : currentSolution.getPizzaDeliveries()) {
            for (PizzaDelivery pizzaDeliverySecond : currentSolution.getPizzaDeliveries()) {
                // current utility of the two considered delvieries
                var currentUtility = utilityCalculator.getUtilityValue(pizzaDeliveryFirst.getPizzas())
                        + utilityCalculator.getUtilityValue(pizzaDeliverySecond.getPizzas());

                // change pizzas
                for (int i=0; i<pizzaDeliveryFirst.getPizzas().size(); i++) {
                    for (int j=0; j<pizzaDeliverySecond.getPizzas().size(); j++) {

                        var alternateFirstDelivery = new ArrayList<Integer>(pizzaDeliveryFirst.getPizzas());
                        var alternateSecondDelivery = new ArrayList<Integer>(pizzaDeliverySecond.getPizzas());
                        alternateFirstDelivery.set(i, pizzaDeliverySecond.getPizzas().get(j));
                        alternateSecondDelivery.set(j, pizzaDeliveryFirst.getPizzas().get(i));

                        var newUtility = utilityCalculator.getUtilityValue(alternateFirstDelivery)
                                + utilityCalculator.getUtilityValue(alternateSecondDelivery);

                        if(newUtility > currentUtility) {
                            pizzaDeliveryFirst.getPizzas().set(i, alternateFirstDelivery.get(i));
                            pizzaDeliverySecond.getPizzas().set(j, alternateSecondDelivery.get(j));
                            continue newSolution;
                        }
                    }
                }
            }
        }
        return currentSolution;
   }


    public List<PizzaDelivery> subProblemLocalSearch(List<PizzaDelivery> pizzaDeliveries){
        List<PizzaDelivery> newPizzaDeliveries = new ArrayList<>(pizzaDeliveries);
        newSolution:
        for (PizzaDelivery pizzaDeliveryFirst : newPizzaDeliveries) {
            for (PizzaDelivery pizzaDeliverySecond : newPizzaDeliveries) {
                // current utility of the two considered delvieries
                var currentUtility = utilityCalculator.getUtilityValue(pizzaDeliveryFirst.getPizzas())
                        + utilityCalculator.getUtilityValue(pizzaDeliverySecond.getPizzas());

                // change pizzas
                for (int i=0; i<pizzaDeliveryFirst.getPizzas().size(); i++) {
                    for (int j=0; j<pizzaDeliverySecond.getPizzas().size(); j++) {

                        var alternateFirstDelivery = new ArrayList<Integer>(pizzaDeliveryFirst.getPizzas());
                        var alternateSecondDelivery = new ArrayList<Integer>(pizzaDeliverySecond.getPizzas());
                        alternateFirstDelivery.set(i, pizzaDeliverySecond.getPizzas().get(j));
                        alternateSecondDelivery.set(j, pizzaDeliveryFirst.getPizzas().get(i));

                        var newUtility = utilityCalculator.getUtilityValue(alternateFirstDelivery)
                                + utilityCalculator.getUtilityValue(alternateSecondDelivery);

                        if(newUtility > currentUtility) {
                            pizzaDeliveryFirst.getPizzas().set(i, alternateFirstDelivery.get(i));
                            pizzaDeliverySecond.getPizzas().set(j, alternateSecondDelivery.get(j));
                            continue newSolution;
                        }
                    }
                }
            }
        }
        return newPizzaDeliveries;
    }
//    public Solution partialLocalSearch(){
//
//        Collections.shuffle(currentSolution.getPizzaDeliveries());
//        int firstIndexMax = Math.min(currentSolution.getPizzaDeliveries().size() - 1, 100000);
//        int secondIndexMax = Math.min(currentSolution.getPizzaDeliveries().size() - 1, 10000);
//
//        newSolution:
//        for (Solution.PizzaDelivery pizzaDeliveryFirst : currentSolution.getPizzaDeliveries().subList(0,firstIndexMax)) {
//            for (Solution.PizzaDelivery pizzaDeliverySecond : currentSolution.getPizzaDeliveries().subList(0,secondIndexMax)) {
//                // current utility of the two considered delvieries
//                var currentUtility = utilityCalculator.getUtilityValue(pizzaDeliveryFirst.getPizzas())
//                        + utilityCalculator.getUtilityValue(pizzaDeliverySecond.getPizzas());
//
//                // change pizzas
//                for (int i=0; i<pizzaDeliveryFirst.getPizzas().size(); i++) {
//                    for (int j=0; j<pizzaDeliverySecond.getPizzas().size(); j++) {
//
//                        var alternateFirstDelivery = new ArrayList<Integer>(pizzaDeliveryFirst.getPizzas());
//                        var alternateSecondDelivery = new ArrayList<Integer>(pizzaDeliverySecond.getPizzas());
//                        alternateFirstDelivery.set(i, pizzaDeliverySecond.getPizzas().get(j));
//                        alternateSecondDelivery.set(j, pizzaDeliveryFirst.getPizzas().get(i));
//
//                        var newUtility = utilityCalculator.getUtilityValue(alternateFirstDelivery)
//                                + utilityCalculator.getUtilityValue(alternateSecondDelivery);
//
//                        if(newUtility > currentUtility) {
//                            pizzaDeliveryFirst.getPizzas().set(i, alternateFirstDelivery.get(i));
//                            pizzaDeliverySecond.getPizzas().set(j, alternateSecondDelivery.get(j));
//                            continue newSolution;
//                        }
//                    }
//                }
//
//            }
//        }
//        return currentSolution;
//    }

}
