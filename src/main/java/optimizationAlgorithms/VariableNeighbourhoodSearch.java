package optimizationAlgorithms;

import eu.katonai.utility.UtilityCalculator;
import simpleSolutions.PizzaDelivery;
import simpleSolutions.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VariableNeighbourhoodSearch{

    private UtilityCalculator utilityCalculator;
    private Solution initialSolution;
    private Solution currentSolution;
    private List<Integer> unusedTeams;
    private List<Integer> unusedPizzas;
    private LocalSearch localSearch;

    public VariableNeighbourhoodSearch(Solution initialSolution) {
        this.utilityCalculator = initialSolution.getUtilityCalculator();
        this.initialSolution = initialSolution;
        this.currentSolution = initialSolution;
        this.unusedTeams = initialSolution.getUnusedTeams();
        this.unusedPizzas = new ArrayList<>();
        this.localSearch = new LocalSearch(initialSolution);
    }

    public Solution performVariableNeighbourhoodSearch(int maxSteps){

        int reorderSize = initialSolution.getPizzaDeliveries().size() / 100;

        for(int step = 0; step < maxSteps; step++){

            if(step % 100 == 0){
                System.out.println(step + "/" + maxSteps);
            }
            Collections.shuffle(currentSolution.getPizzaDeliveries());
            var reorderSection = currentSolution.getPizzaDeliveries().subList(0, reorderSize);
            var oldUtility = currentSolution.getUtilityCalculator().getUtility(reorderSection);

            for (PizzaDelivery pizzaDelivery : reorderSection) {
                unusedTeams.add(pizzaDelivery.getTeamMembers());
                unusedPizzas.addAll(pizzaDelivery.getPizzas());
            }

            Collections.shuffle(unusedTeams);
            Collections.shuffle(unusedPizzas);

            List<PizzaDelivery> newDelivery = new ArrayList<>();

            for(Integer team : unusedTeams){
                if(unusedPizzas.size() >= team){
                    newDelivery.add(new PizzaDelivery(team, new ArrayList<>(unusedPizzas.subList(0, team))));
                    unusedPizzas.subList(0, team).clear();
                } else {
                    break;
                }
            }

            var optimalNewDelivery = localSearch.subProblemLocalSearch(newDelivery);
            if(currentSolution.getUtilityCalculator().getUtility(optimalNewDelivery) > oldUtility){
                currentSolution.getPizzaDeliveries().subList(0, reorderSize).clear();
                currentSolution.getPizzaDeliveries().addAll(optimalNewDelivery);
            }
            this.unusedTeams = currentSolution.getUnusedTeams();
            this.unusedPizzas = new ArrayList<>();
        }

        return currentSolution;
    }





}
