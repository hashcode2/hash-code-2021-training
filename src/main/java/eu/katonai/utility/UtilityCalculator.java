package eu.katonai.utility;

import eu.kateonai.input.model.PizzaProblem;
import eu.kateonai.input.model.PizzaProblemModel;
import simpleSolutions.PizzaDelivery;
import simpleSolutions.Solution;

import java.util.*;
import java.util.stream.Collectors;

public class UtilityCalculator {

    private Map<Integer, List<String>> pizzaMap;

    public UtilityCalculator(PizzaProblemModel pizzaProblemModel) {
        pizzaMap = new HashMap<>();
        for (PizzaProblem pizza : pizzaProblemModel.getPizzaProblem()) {
            pizzaMap.put(pizza.getIndex(), pizza.getIngredients());
        }
    }

    public int getUtilityValue(Integer ... pizzaIds){

        var utility = 0;
        var uniqueIngredients = new HashSet<>();
        for (int pizzaId : pizzaIds) {
            uniqueIngredients.addAll(pizzaMap.get(pizzaId));
        }
        utility = uniqueIngredients.size() * uniqueIngredients.size();
        return utility;
    }

    public int getUtilityValue(List<Integer> pizzaIds){
        var utility = 0;
        var uniqueIngredients = new HashSet<>();
        for (int pizzaId : pizzaIds) {
            uniqueIngredients.addAll(pizzaMap.get(pizzaId));
        }
        utility = uniqueIngredients.size() * uniqueIngredients.size();
        return utility;
    }

    public int getUtility(Solution solution){
        var utility = 0;
        for (PizzaDelivery pizzaDelivery : solution.getPizzaDeliveries()) {
            utility += getUtilityValue(pizzaDelivery.getPizzas());
        }
        return utility;
    }

    public int getUtility(List<PizzaDelivery> pizzaDeliveries){
        var utility = 0;
        for(PizzaDelivery pizzaDelivery : pizzaDeliveries){
            utility += getUtilityValue(pizzaDelivery.getPizzas());
        }
        return utility;
    }
}
