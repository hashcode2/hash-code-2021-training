package simpleSolutions;

import eu.kateonai.input.model.PizzaProblemModel;

import java.util.*;

public class RandomSolution extends Solution{

    public RandomSolution(PizzaProblemModel pizzaProblemModel){

        super(pizzaProblemModel);

        var availablePizzaIndex = new ArrayList<Integer>();
        for(int i=0; i<pizzaProblemModel.getPizzaProblemConfig().getPizzaCount(); i++){
            availablePizzaIndex.add(i);
        }
        Collections.shuffle(availablePizzaIndex);

        var availableTeams = this.getAvailableTeams();
        Collections.shuffle(availableTeams);

        for(Integer team : availableTeams){
            if(availablePizzaIndex.size() >= team){
                this.getPizzaDeliveries().add(new PizzaDelivery(team, new ArrayList<>(availablePizzaIndex.subList(0, team))));
                availablePizzaIndex.subList(0, team).clear();
            }
        }
        this.setServedTeams(this.getPizzaDeliveries().size());
    }
}

