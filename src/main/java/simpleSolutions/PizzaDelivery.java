package simpleSolutions;

import java.util.Arrays;
import java.util.List;

public class PizzaDelivery {

    private int teamMembers;
    private List<Integer> pizzas;

    public PizzaDelivery(int teamMembers, List<Integer> pizzas) {
        this.teamMembers = teamMembers;
        this.pizzas = pizzas;
    }

    public PizzaDelivery(int teamMembers, Integer ... pizzas){
        this.teamMembers = teamMembers;
        this.pizzas = Arrays.asList(pizzas);
    }

    public int getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(int teamMembers) {
        this.teamMembers = teamMembers;
    }

    public List<Integer> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Integer> pizzas) {
        this.pizzas = pizzas;
    }

    @Override
    public String toString() {
        String string = teamMembers + " ";
        for(Integer pizza : pizzas){
            string += pizza + " ";
        }
        return string;
    }
}
