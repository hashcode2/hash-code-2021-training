package simpleSolutions;

import eu.kateonai.input.model.PizzaProblemModel;
import eu.katonai.utility.UtilityCalculator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {

    private PizzaProblemModel pizzaProblemModel;
    private UtilityCalculator utilityCalculator;
    private int servedTeams = 0;
    private List<PizzaDelivery> pizzaDeliveries = new ArrayList<>();

    public Solution(PizzaProblemModel pizzaProblemModel) {
        this.pizzaProblemModel = pizzaProblemModel;
        this.utilityCalculator = new UtilityCalculator(pizzaProblemModel);
    }

    public int getServedTeams() {
        return servedTeams;
    }

    public void setServedTeams(int servedTeams) {
        this.servedTeams = servedTeams;
    }

    public List<PizzaDelivery> getPizzaDeliveries() {
        return pizzaDeliveries;
    }

    public void setPizzaDeliveries(List<PizzaDelivery> pizzaDeliveries) {
        this.pizzaDeliveries = pizzaDeliveries;
    }

    public void toFile(Integer index){

        this.setServedTeams(this.pizzaDeliveries.size());
        String stringRepresentation = this.getServedTeams() + "\n";
        for(PizzaDelivery pizzaDelivery : this.getPizzaDeliveries()){
            stringRepresentation += pizzaDelivery.toString() + "\n";
        }
        try {
            var outputFile = new FileWriter("output_"+index);
            System.out.println("Write Solution to output_" + index);
            outputFile.write(stringRepresentation);
            outputFile.flush();
            outputFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PizzaProblemModel getPizzaProblemModel() {
        return pizzaProblemModel;
    }

    public UtilityCalculator getUtilityCalculator() {
        return utilityCalculator;
    }

    public int calculateUtility(){
        return utilityCalculator.getUtility(this.pizzaDeliveries);
    }

    public List<Integer> getAvailableTeams(){
        List<Integer> availableTeams = new ArrayList<>();
        for (int i=0; i<pizzaProblemModel.getPizzaProblemConfig().getTwoPersonTeams(); i++){
            availableTeams.add(2);
        }
        for (int i=0; i<pizzaProblemModel.getPizzaProblemConfig().getThreePersonTeams(); i++){
            availableTeams.add(3);
        }
        for (int i=0; i<pizzaProblemModel.getPizzaProblemConfig().getFourPersonTeams(); i++){
            availableTeams.add(4);
        }
        return availableTeams;
    }

    public List<Integer> getUnusedTeams(){
        var numTwoPersonTeams = this.pizzaProblemModel.getPizzaProblemConfig().getTwoPersonTeams();
        var numThreePersonTeams = this.pizzaProblemModel.getPizzaProblemConfig().getThreePersonTeams();
        var numFourPersonTeams = this.pizzaProblemModel.getPizzaProblemConfig().getFourPersonTeams();
        for (PizzaDelivery pizzaDelivery : this.getPizzaDeliveries()) {
            if(pizzaDelivery.getTeamMembers() == 2){
                numTwoPersonTeams --;
            }
            if(pizzaDelivery.getTeamMembers() == 3){
                numThreePersonTeams --;
            }
            if(pizzaDelivery.getTeamMembers() == 4){
                numFourPersonTeams --;
            }
        }
        List<Integer> unusedTeams = new ArrayList<>();
        for (int i=0; i<numTwoPersonTeams; i++){
            unusedTeams.add(2);
        }
        for (int i=0; i<numThreePersonTeams; i++){
            unusedTeams.add(3);
        }
        for (int i=0; i<numFourPersonTeams; i++){
            unusedTeams.add(4);
        }
        return unusedTeams;

    }
}
