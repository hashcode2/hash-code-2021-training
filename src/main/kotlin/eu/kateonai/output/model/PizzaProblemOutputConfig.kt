package eu.kateonai.output.model

import eu.kateonai.config.Order

class PizzaProblemOutputConfig {
    @Order(0)
    var servedTeams: Int = 0
}