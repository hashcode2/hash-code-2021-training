package eu.kateonai.output.model

import eu.kateonai.config.BaseModel
import eu.kateonai.config.MetaConfig
import eu.kateonai.config.OtherLines

class PizzaProblemOutputModel : BaseModel {
    @MetaConfig
    var config: PizzaProblemOutputConfig? = null

    @OtherLines
    var deliveries: List<PizzaDelivery> = mutableListOf()
}