package eu.kateonai.output.model

import eu.kateonai.config.Order
import eu.kateonai.config.OtherValues

class PizzaDelivery {
    @Order(0)
    var teamMembers = 0

    @OtherValues
    var pizzas: List<Int> = mutableListOf()
}