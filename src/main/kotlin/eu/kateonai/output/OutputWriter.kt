package eu.kateonai.output

import eu.kateonai.config.*
import java.io.File
import java.lang.IllegalArgumentException
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties

class OutputWriter<T : BaseModel> {
    fun write(filePath: String, model: T) {
        val memberProperties = model::class.members.filterIsInstance<KMutableProperty<*>>()
        val config = memberProperties.find { it.hasAnnotation<MetaConfig>() }
        val lines = memberProperties.find { it.hasAnnotation<OtherLines>() }!!
        val configInstance = config?.getter?.call(model)
        // open file
        File(filePath).bufferedWriter().use { writer ->
            // write config line
            if (configInstance != null) {
                val configProperties = (config.returnType.classifier as KClass<*>).members
                    .filterIsInstance<KMutableProperty<*>>()
                    .filter { it.hasAnnotation<Order>() }
                    .sortedBy { (it.annotations.first { ann -> ann.annotationClass == Order::class } as Order).order }
                val configLine = configProperties.joinToString(ParserConfig.SEPARATOR) {
                    it.getter.call(configInstance).toString()
                }.plus("\n")
                writer.write(configLine)
            }
            // write other lines
            val linesInstance = lines.getter.call(model)
            if (linesInstance is Collection<*>) {
                val lineType = lines.returnType.arguments.first().type!!
                val lineProperties = (lineType.classifier as KClass<*>).memberProperties
                    .filterIsInstance<KMutableProperty<*>>()
                val orderedLineProperties = lineProperties
                    .filter { it.hasAnnotation<Order>() }
                    .sortedBy { (it.annotations.first { ann -> ann.annotationClass == Order::class } as Order).order }
                val otherValuesProperty = lineProperties
                    .firstOrNull { it.hasAnnotation<OtherValues>() }
                linesInstance.forEach { line ->
                    val orderLinePart = orderedLineProperties
                        .joinToString(ParserConfig.SEPARATOR)  { prop ->
                            prop.getter.call(line).toString()
                        }
                    val otherValuesLinePart = if(otherValuesProperty != null) {
                        val otherValuesInstance = otherValuesProperty.getter.call(line)
                        if(otherValuesInstance is Collection<*>) {
                            otherValuesInstance.joinToString(ParserConfig.SEPARATOR) { value ->
                                value.toString()
                            }
                        } else {
                            throw IllegalArgumentException("OtherValues has to be a collection!")
                        }
                    } else {
                        ""
                    }
                    val fullLine = if(orderLinePart != "") {
                        "${orderLinePart}${ParserConfig.SEPARATOR}${otherValuesLinePart}"
                    } else {
                        otherValuesLinePart
                    }.plus("\n")
                    writer.write(fullLine)
                    writer.flush()
                }
            } else {
                throw IllegalArgumentException("@OtherLines has to be a collection!")
            }
        }
    }
}