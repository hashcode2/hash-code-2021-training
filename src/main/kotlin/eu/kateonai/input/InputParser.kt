package eu.kateonai.input

import eu.kateonai.config.*
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KType
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

class InputParser<T : BaseModel> {
    fun parse(filePath: String, modelFactory: () -> T): T {
        val model = modelFactory()
        val memberProperties = model::class.members.filterIsInstance<KMutableProperty<*>>()
        val config = memberProperties.find { it.hasAnnotation<MetaConfig>() }
        val fields = memberProperties.find { it.hasAnnotation<OtherLines>() }!!
        val configInstance = if (config != null) {
            (config.returnType.classifier as KClass<*>).primaryConstructor!!.call()
        } else {
            null
        }
        // set config
        val configProperties = if (config != null) {
            (config.returnType.classifier as KClass<*>).memberProperties
        } else {
            null
        }
        val fieldType = fields.returnType.arguments.first().type!!.classifier as KClass<*>
        val fieldProperties = fieldType.memberProperties
        val fieldList = mutableListOf<Any>()
        var configInitialized = false
        val reader = BufferedReader(InputStreamReader(File(filePath).inputStream()))
        var rowIndex = -1
        reader.use { r ->
            r.forEachLine { line ->
                var elements = line.split(ParserConfig.SEPARATOR)
                elements = elements.filter { it != "" }
                if (!configInitialized && configInstance != null) {
                    elements.forEachIndexed { index, element ->
                        val prop =
                            configProperties!!.first { (it.annotations.first { ann -> ann.annotationClass == Order::class } as Order).order == index } as KMutableProperty<*>
                        val propValue = convertToTargetType(element, prop.returnType)
                        prop.setter.call(configInstance, propValue)
                    }
                    configInitialized = true
                } else {
                    val fieldInstance = fieldType.primaryConstructor!!.call()
                    elements.forEachIndexed { index, element ->
                        val indexProp =
                            fieldProperties.firstOrNull { it.annotations.firstOrNull { ann -> ann.annotationClass == Index::class } != null } as? KMutableProperty<*>
                        indexProp?.setter?.call(fieldInstance, rowIndex)
                        var prop =
                            fieldProperties.firstOrNull { (it.annotations.firstOrNull { ann -> ann.annotationClass == Order::class } as? Order)?.order == index } as? KMutableProperty<*>
                        if(prop == null) {
                            prop =
                                fieldProperties.firstOrNull { (it.annotations.firstOrNull { ann -> ann.annotationClass == Order::class } as? Order)?.list == true } as KMutableProperty<*>
                        }
                        if(prop.returnType.classifier as KClass<*> == MutableList::class) {
                            val listProp = prop.getter.call(fieldInstance)
                            val propValue = convertToTargetType(element, prop.returnType.arguments.first().type!!)
                            (listProp as MutableList<Any>).add(propValue)
                        } else {
                            val propValue = convertToTargetType(element, prop.returnType)
                            prop.setter.call(fieldInstance, propValue)
                            fieldList.add(fieldInstance)
                        }
                    }
                }
                rowIndex++
            }
        }
        // write to model
        config?.setter?.call(model, configInstance)
        fields.setter.call(model, fieldList)
        return model
    }

    private fun convertToTargetType(field: String, targetType: KType): Any {
        return when (targetType.classifier) {
            Int::class -> field.toInt()
            Long::class -> field.toLong()
            Double::class -> field.toDouble()
            String::class -> field
            else -> throw IllegalArgumentException("Can't parse primitive type to $targetType")
        }
    }
}