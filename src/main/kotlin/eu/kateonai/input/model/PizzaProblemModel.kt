package eu.kateonai.input.model

import eu.kateonai.config.BaseModel
import eu.kateonai.config.MetaConfig
import eu.kateonai.config.OtherLines

class PizzaProblemModel : BaseModel {
    @MetaConfig
    lateinit var pizzaProblemConfig: PizzaProblemConfig

    @OtherLines
    lateinit var pizzaProblem: List<PizzaProblem>
}