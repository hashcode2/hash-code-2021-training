package eu.kateonai.input.model

import eu.kateonai.config.Index
import eu.kateonai.config.Order

class PizzaProblem {
    @Index
    var index: Int = 0

    @Order(0)
    var ingredientsCount: Int = 0

    @Order(1, true)
    var  ingredients: List<String> = mutableListOf()
}