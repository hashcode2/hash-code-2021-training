package eu.kateonai.input.model

import eu.kateonai.config.Order

class PizzaProblemConfig {
    @Order(0)
    var pizzaCount: Int = 0

    @Order(1)
    var twoPersonTeams: Int = 0

    @Order(2)
    var threePersonTeams: Int = 0

    @Order(3)
    var fourPersonTeams: Int = 0
}