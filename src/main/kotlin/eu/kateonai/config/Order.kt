package eu.kateonai.config

@Target(AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.RUNTIME)
annotation class Order(val order: Int, val list: Boolean = false)
